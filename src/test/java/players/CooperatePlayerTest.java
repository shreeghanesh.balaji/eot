package players;

import org.junit.Before;
import org.junit.Test;
import players.CooperatePlayer;
import players.IPlayer;

import static org.junit.Assert.assertEquals;

public class CooperatePlayerTest {

    private IPlayer cooperatePlayer;

    @Before
    public void setup() {
        cooperatePlayer = new CooperatePlayer();
    }

    @Test
    public void makeMoveShouldReturnCO() {
        assertEquals("co", cooperatePlayer.makeMove());
    }

}