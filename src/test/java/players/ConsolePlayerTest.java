package players;

import inputreader.IScanner;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import players.ConsolePlayer;
import players.IPlayer;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class ConsolePlayerTest {

    private IPlayer consolePlayer;

    @Mock
    IScanner iScanner;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        consolePlayer = new ConsolePlayer(iScanner);
    }

    @Test
    public void verifyMoveValueWhenMoveIsSetToCheat() {
        when(iScanner.readLine()).thenReturn("ch");

        assertEquals("ch", consolePlayer.makeMove());
    }
}
