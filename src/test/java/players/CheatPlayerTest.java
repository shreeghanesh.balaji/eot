package players;

import org.junit.Before;
import org.junit.Test;
import players.CheatPlayer;
import players.IPlayer;

import static org.junit.Assert.assertEquals;

public class CheatPlayerTest {

    private IPlayer cheatPlayer;

    @Before
    public void setup() {
        cheatPlayer = new CheatPlayer();
    }

    @Test
    public void makeMoveShouldReturnCO() {
        assertEquals("ch", cheatPlayer.makeMove());
    }
}