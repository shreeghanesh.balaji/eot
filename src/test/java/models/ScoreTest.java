package models;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ScoreTest {

    private Score score;

    @Before
    public void setup() {
        score = new Score(0, 0);
    }

    @Test
    public void shouldUpdateScoreTo1and1ForBothPlayers() {
        score.updateScore(new Score(1, 1));

        assertEquals(1, score.getPlayer1Score());
        assertEquals(1, score.getPlayer2Score());
    }

    @Test
    public void shouldReturnCurrentScore() {
        score.updateScore(new Score(1, 1));

        assertEquals("The current score for Player 1:1 and player 2 is:1", score.currentScore());
    }
}