import models.Move;
import models.Score;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MachineTest {
    private Machine machine;

    @Before
    public void setup() {
        machine = new Machine();
    }

    @Test
    public void processScoreWhenPlayer1CheatsAndPlayer2Cooperates() {
        assertEquals(new Score(3, -1), machine.processScore(Move.CHEAT, Move.COOPERATE));
    }

    @Test
    public void processScoreWhenPlayer2CheatsAndPlayer1Cooperates() {
        assertEquals(new Score(-1, 3), machine.processScore(Move.COOPERATE, Move.CHEAT));
    }

    @Test
    public void processScoreWhenPlayer1CooperatesAndPlayer2Cooperates() {
        assertEquals(new Score(2, 2), machine.processScore(Move.COOPERATE, Move.COOPERATE));
    }

    @Test
    public void processScoreWhenPlayer1CheatsAndPlayer2Cheats() {
        assertEquals(new Score(0, 0), machine.processScore(Move.CHEAT, Move.CHEAT));
    }
}
