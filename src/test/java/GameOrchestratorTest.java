import players.IPlayer;
import inputreader.IScannerImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static models.Move.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class GameOrchestratorTest {
    private GameOrchestrator game;

    @Mock
    private IScannerImpl inputReader;

    @Mock
    private IPlayer player1;

    @Mock
    private IPlayer player2;

    @Before
    public void setupPlayerData() {
        MockitoAnnotations.initMocks(this);
        game = new GameOrchestrator(player1, player2, inputReader);
    }

    @Test
    public void processScoreWhenPlayer1CheatsAndPlayer2CooperatesWhenBothAreConsolePlayers() {
        when(player1.makeMove()).thenReturn(CHEAT.getMove());
        when(player2.makeMove()).thenReturn(COOPERATE.getMove());

        game.playGame(1);

        verify(inputReader).printInConsole("The current score for Player 1:3 and player 2 is:-1");
    }

    @Test
    public void processScoreWhenPlayer1CooperatesAndPlayer2CooperatesWhenBothAreConsolePlayers() {
        game = new GameOrchestrator(player1, player2, inputReader);
        when(player1.makeMove()).thenReturn(COOPERATE.getMove());
        when(player2.makeMove()).thenReturn(COOPERATE.getMove());

        game.playGame(1);

        verify(inputReader).printInConsole("The current score for Player 1:2 and player 2 is:2");
    }

    @Test
    public void shouldUpdateScoreFor2RoundsWhenBothAreConsolePlayers() {
        when(player1.makeMove()).thenReturn(COOPERATE.getMove()).thenReturn(CHEAT.getMove());
        when(player2.makeMove()).thenReturn(COOPERATE.getMove()).thenReturn(COOPERATE.getMove());

        game.playGame(2);

        verify(inputReader).printInConsole("The current score for Player 1:5 and player 2 is:1");
    }

    @Test
    public void shouldReturnZeroScoreIfEitherInputIsInvalid() {
        when(player1.makeMove()).thenReturn(INVALID.getMove());
        when(player2.makeMove()).thenReturn(CHEAT.getMove());


        game.playGame(1);

        verify(inputReader).printInConsole("The current score for Player 1:0 and player 2 is:0");
    }

    @Test
    public void shouldUpdateScoreForOneRoundForAlwaysCheatPlayerAndAlwaysCooperatePlayer() {
        when(player1.makeMove()).thenReturn(CHEAT.getMove());
        when(player2.makeMove()).thenReturn(COOPERATE.getMove());

        game.playGame(1);

        verify(inputReader).printInConsole("The current score for Player 1:3 and player 2 is:-1");
    }

    @Test
    public void shouldUpdateScoreAfterFiveRoundsForAlwaysCheatPlayerAndAlwaysCooperatePlayer4() {
        when(player1.makeMove()).thenReturn(CHEAT.getMove());
        when(player2.makeMove()).thenReturn(COOPERATE.getMove());

        game.playGame(5);

        verify(inputReader).printInConsole("The current score for Player 1:15 and player 2 is:-5");
    }

    @Test
    public void shouldUpdateScoreForAlwaysCheatAndConsolePlayerForThreeRounds() {
        when(player1.makeMove()).thenReturn(CHEAT.getMove());
        when(player2.makeMove()).thenReturn(COOPERATE.getMove()).thenReturn(CHEAT.getMove()).thenReturn(COOPERATE.getMove());

        game.playGame(3);

        verify(inputReader).printInConsole("The current score for Player 1:6 and player 2 is:-2");
    }
}
