package players;

public interface IPlayer {
    public String makeMove();
}
