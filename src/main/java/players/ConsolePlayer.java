package players;

import inputreader.IScanner;

public class ConsolePlayer implements IPlayer {
    private IScanner iScanner;

    public ConsolePlayer(IScanner iScanner) {
        this.iScanner = iScanner;
    }

    @Override
    public String makeMove() {
        iScanner.printInConsole("Enter input for player:");
        return iScanner.readLine();
    }
}
