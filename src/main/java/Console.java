import players.CheatPlayer;
import players.ConsolePlayer;
import inputreader.IScannerImpl;

public class Console {
    public static void main(String[] args) {
        IScannerImpl inputReader = new IScannerImpl();
        System.out.println("Welcome to EOT game....");
        GameOrchestrator gameOrchestrator = new GameOrchestrator(new ConsolePlayer(inputReader), new CheatPlayer(), inputReader);
        gameOrchestrator.playGame(5);
    }
}
