import models.Move;
import models.Score;

public class Machine {
    public Score processScore(Move player1Move, Move player2move) {
        if (player1Move.equals(Move.COOPERATE) && player2move.equals(Move.COOPERATE)) {
            return new Score(2, 2);
        }
        if (player1Move.equals(Move.CHEAT) && player2move.equals(Move.CHEAT)) {
            return new Score(0, 0);
        }
        if (player1Move.equals(Move.COOPERATE) && player2move.equals(Move.CHEAT)) {
            return new Score(-1, 3);
        }
        if (player1Move.equals(Move.CHEAT) && player2move.equals(Move.COOPERATE)) {
            return new Score(3, -1);
        } else return new Score(0, 0);
    }
}
