package inputreader;

public interface IScanner {
    public String readLine();
    public void printInConsole(String input);
}
