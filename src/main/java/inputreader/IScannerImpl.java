package inputreader;

import java.util.Scanner;

public class IScannerImpl implements IScanner {
    private Scanner scanner = new Scanner(System.in);

    @Override
    public String readLine() {
        return scanner.nextLine();
    }

    @Override
    public void printInConsole(String input) {
        System.out.println(input);
    }
}
