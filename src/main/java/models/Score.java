package models;

import java.util.Objects;

public class Score {
    private int player1Score;
    private int player2Score;

    public Score(int player1Score, int player2Score) {
        this.player1Score = player1Score;
        this.player2Score = player2Score;
    }

    public int getPlayer1Score() {
        return player1Score;
    }

    public int getPlayer2Score() {
        return player2Score;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Score score = (Score) o;
        return player1Score == score.player1Score &&
                player2Score == score.player2Score;
    }

    @Override
    public int hashCode() {
        return Objects.hash(player1Score, player2Score);
    }

    public void updateScore(Score score) {
        this.player1Score += score.player1Score;
        this.player2Score += score.player2Score;
    }

    public String currentScore() {
        return "The current score for Player 1:" + player1Score + " and player 2 is:" + player2Score;
    }
}
