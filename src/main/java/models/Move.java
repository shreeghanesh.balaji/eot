package models;

public enum Move {
    CHEAT("ch"), COOPERATE("co"), INVALID("");

    private String move;

    Move(String move) {
        this.move = move;
    }

    public String getMove() {
        return move;
    }
}
