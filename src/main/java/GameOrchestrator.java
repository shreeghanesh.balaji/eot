import inputreader.IScannerImpl;
import models.*;
import players.IPlayer;

import static models.Move.CHEAT;
import static models.Move.COOPERATE;

public class GameOrchestrator {
    private IPlayer player1;
    private IPlayer player2;
    private Machine machine;
    private IScannerImpl inputReader;
    private Score scoreBoard;

    public GameOrchestrator(IPlayer player1, IPlayer player2, IScannerImpl inputReader) {
        this.player1 = player1;
        this.player2 = player2;
        this.machine = new Machine();
        this.inputReader = inputReader;
        scoreBoard = new Score(0, 0);
    }

    public void playGame(int rounds) {
        for (int i = 0; i < rounds; i++) {
            Move player1Move = validateUserMove(player1.makeMove());
            Move player2Move = validateUserMove(player2.makeMove());
            calculateScoreAfterOneRound(player1Move, player2Move);
            inputReader.printInConsole(scoreBoard.currentScore());
        }
    }

    private void calculateScoreAfterOneRound(Move player1Move, Move player2Move) {
        Score score = machine.processScore(player1Move, player2Move);
        scoreBoard.updateScore(score);
    }

    private Move validateUserMove(String input) {
        if (input.equals(CHEAT.getMove()))
            return CHEAT;
        else if (input.equals(COOPERATE.getMove()))
            return Move.COOPERATE;
        else return Move.INVALID;
    }
}
